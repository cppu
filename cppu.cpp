/***
 * CppU - C++ unit testing framework
 * ---------------------------------
 * Copyright (c)2007 Daniel Fiser <danfis@danfis.cz>
 *
 *
 *  This file is part of CppU.
 *
 *  CppU is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  CppU is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file cppu.cpp
 * Compile this file as object file (e.g.\ gcc -c -o cppu.o cppu.c) and
 * link the object file with your test suite.
 */


#include <iostream>
#include <sstream>
#include "cppu.h"
using namespace std;

/**************  TestBase  **************/
void TestBase::printSummary(std::ostream& out) const
{
    out << endl;
    out << "==================================================" << endl;
    out << "|               |  failed  |  succeed  |  total  |" << endl;
    out << "|------------------------------------------------|" << endl;
    out << "| assertations: |  ";
      out.width(6);
      out << left << assertationsFailed();
      out << "  |  ";
      out.width(7);
      out << left << assertationsSucceed();
      out << "  |  ";
      out.width(5);
      out << left << assertations();
      out << "  |" << endl;
    out << "| tests:        |  ";
      out.width(6);
      out << left << testsFailed();
      out << "  |  ";
      out.width(7);
      out << left << testsSucceed();
      out << "  |  ";
      out.width(5);
      out << left << tests();
      out << "  |" << endl;
    out << "| test cases:   |  ";
      out.width(6);
      out << left << testCasesFailed();
      out << "  |  ";
      out.width(7);
      out << left << testCasesSucceed();
      out << "  |  ";
      out.width(5);
      out << left << testCases();
      out << "  |" << endl;
    out << "| test suites:  |  ";
      out.width(6);
      out << left << testSuitesFailed();
      out << "  |  ";
      out.width(7);
      out << left << testSuitesSucceed();
      out << "  |  ";
      out.width(5);
      out << left << testSuites();
      out << "  |" << endl;
    out << "==================================================" << endl;
}
/**************  TestBase END  **********/


/**************  TestCase  **************/
void TestCase::run(ostream &out, const int level)
{
    this->_out = &out;

	(*_out) << string(2*level, ' ') << "=> " << "started test case " << _name << endl;

    setUp();
    runTests();
    tearDown();

	(*_out) << string(2*level, ' ') << "<= " << "ended test case " << _name << endl;
}


//protected:
string TestCase::_getErrorMessage(const string file, const int line,
        const string test_method, const string error_message) const
{
    std::stringstream ss;
    std::string l;
    ss << line;
    ss >> l;
    return (file + ":" + l + ": " + _name + "::" + test_method + " :: " + error_message);
}

void TestCase::_prepareTest(string name)
{
    _current_test.clean();
    _current_test.test_name = name;
}

void TestCase::_finishTest(void)
{
    _assertations_failed += _current_test.fails;
    _assertations_succeed += _current_test.successes;

    if (_current_test.fails > 0){
        _tests_failed++;
    }else{
        _tests_succeed++;
    }
}


void TestCase::_successAssertation(void)
{
    _current_test.successes++;
}

void TestCase::_failAssertation(string file, int line,
        string error_message)
{
    _current_test.fails++;
    (*_out) << _getErrorMessage(file, line, _current_test.test_name,
                                error_message)
            << endl;
}
/**************  TestCase END  **********/



/**************  TestSuite  *************/
TestSuite::~TestSuite()
{
    _freeTests();
}

void TestSuite::run(ostream &out, const int level)
{
    _freeTests();
    _registerAll();

	out << string(2*level, ' ') << "=> " << "started test suite " << _name << endl;

    vector<TestBase *>::iterator it = _tests.begin();
    vector<TestBase *>::iterator it_end = _tests.end();
    for (;it != it_end; it++){
        (*it)->run(out, level+1);
    }

	out << string(2*level, ' ') << "=> " << "ended test suite " << _name << endl;
}


int TestSuite::assertations() const
{
    int num = 0;

    vector<TestBase *>::const_iterator it = _tests.begin();
    vector<TestBase *>::const_iterator it_end = _tests.end();
    for (;it != it_end; it++){
        num += (*it)->assertations();
    }

    return num;
}
int TestSuite::assertationsFailed() const
{
    int num = 0;

    vector<TestBase *>::const_iterator it = _tests.begin();
    vector<TestBase *>::const_iterator it_end = _tests.end();
    for (;it != it_end; it++){
        num += (*it)->assertationsFailed();
    }

    return num;
}
int TestSuite::assertationsSucceed() const
{
    return assertations() - assertationsFailed();
}

int TestSuite::tests() const
{
    int num = 0;

    vector<TestBase *>::const_iterator it = _tests.begin();
    vector<TestBase *>::const_iterator it_end = _tests.end();
    for (;it != it_end; it++){
        num += (*it)->tests();
    }

    return num;
}
int TestSuite::testsFailed() const
{
    int num = 0;

    vector<TestBase *>::const_iterator it = _tests.begin();
    vector<TestBase *>::const_iterator it_end = _tests.end();
    for (;it != it_end; it++){
        num += (*it)->testsFailed();
    }

    return num;
}
int TestSuite::testsSucceed() const
{
    return tests() - testsFailed();
}

int TestSuite::testCases() const
{
    int num = 0;

    vector<TestBase *>::const_iterator it = _tests.begin();
    vector<TestBase *>::const_iterator it_end = _tests.end();
    for (;it != it_end; it++){
        if ((*it)->isTestCase()){
            num ++;
        }else if ((*it)->isTestSuite()){
            num += (*it)->testCases();
        }
    }

    return num;
}
int TestSuite::testCasesFailed() const
{
    int num = 0;

    vector<TestBase *>::const_iterator it = _tests.begin();
    vector<TestBase *>::const_iterator it_end = _tests.end();
    for (;it != it_end; it++){
        if ((*it)->isTestCase()){
            if ((*it)->testsFailed() > 0)
                num ++;
        }else if ((*it)->isTestSuite()){
            num += (*it)->testCasesFailed();
        }
    }

    return num;
}
int TestSuite::testCasesSucceed() const
{
    return testCases() - testCasesFailed();
}

int TestSuite::testSuites() const
{
    int num = 0;

    vector<TestBase *>::const_iterator it = _tests.begin();
    vector<TestBase *>::const_iterator it_end = _tests.end();
    for (;it != it_end; it++){
        if ((*it)->isTestSuite()){
            num += 1 + (*it)->testSuites();
        }
    }

    return num;
}
int TestSuite::testSuitesFailed() const
{
    int num = 0;

    vector<TestBase *>::const_iterator it = _tests.begin();
    vector<TestBase *>::const_iterator it_end = _tests.end();
    for (;it != it_end; it++){
        if ((*it)->isTestSuite()){
            if ((*it)->testCasesFailed() > 0
                || (*it)->testSuitesFailed() > 0)
                num++;
            num += (*it)->testSuitesFailed();
        }
    }

    return num;
}
int TestSuite::testSuitesSucceed() const
{
    return testSuites() - testSuitesFailed();
}

//protected:
void TestSuite::_freeTests()
{
    vector<TestBase *>::iterator it = _tests.begin();
    vector<TestBase *>::iterator it_end = _tests.end();
    for (;it != it_end; it++){
        delete (*it);
    }
    _tests.clear();
}
/**************  TestSuite END  *********/

void runTest(TestBase * test, ostream &out)
{
    test->run(out);
    test->printSummary(out);
    delete test;
}
