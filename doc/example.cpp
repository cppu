// example.cpp

#include <iostream>
#include <fstream>
using namespace std;

#include "cppu.h"

TEST_CASE (TC1); // create test case
	void testMethod() // define test method
	{
        assertEquals(1,1);
        assertEquals(2,1);
	}

	void testMethod2()
	{
        assertEquals(1,1);
        assertEquals(2,1);
        assertThrow(throw string("Ahoj"), vector<int>);
	}
	void testMethod3()
	{
        assertEquals(1,1);
        assertThrow(throw string("Ahoj"), string&);
	}

    TESTS
    {
        // register test methods:
        REG_TEST(testMethod);
        REG_TEST(testMethod2);
        REG_TEST(testMethod3);
    }
TEST_CASE_END;


TEST_CASE(TC2); // second test case
	void testMethod4()
	{
        assertEquals(1,1);
        assertEqualsM(2,1,"2 not equals 1, dude");
	}
	void testMethod5()
	{
        assertEquals(1,1);
	}

	void testMethod6()
	{
        assertEquals(1,1);
        assertNotEquals(2,1);
	}

	TESTS
	{
		REG_TEST(testMethod4);
		REG_TEST(testMethod5);
		REG_TEST(testMethod6);
	}
TEST_CASE_END;


TEST_CASE(TC3);
    void testMethod2()
    {
        assertFalse(false);
    }
    void testMethod()
    {
        assertTrue(true);
    }

    TESTS
    {
        REG_TEST(testMethod);
        REG_TEST(testMethod2);
    }
TEST_CASE_END;


TEST_SUITE(TS1); // create test suite
    REG(TC1); // register test case
    REG(TC2);
TEST_SUITE_END;

TEST_SUITE(TS2);
    REG(TC3); 
TEST_SUITE_END;

TEST_SUITE(TS3);
    REG(TS1); // register test suite
    REG(TC2); // register test case
TEST_SUITE_END;


int main(int argc, char *argv[])
{
    RUN(TC1); // run test case
	cout << endl << "++++++++++++++++++++" << endl;
    RUN(TC2);
	cout << endl << "++++++++++++++++++++" << endl;
    RUN(TC3); // run test suite
	cout << endl << "++++++++++++++++++++" << endl;
    RUN(TS1);
	cout << endl << "++++++++++++++++++++" << endl;
    RUN(TS2);
	cout << endl << "++++++++++++++++++++" << endl;
    RUN(TS3);
	cout << endl << "++++++++++++++++++++" << endl;

    ofstream fs("/tmp/test.out");
    RUN_OUT(TS3, fs); // run test suite and print output into fs
    fs.close();

	return 0;
}
