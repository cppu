/**
 * \mainpage C++ unit testing framework
 *
 * \section writing How to write test suite
 * \include example.cpp
 * \endsection
 *
 * \section compiling How to compile and run test suite
 * <code><b>$ ls</b></code>
 * \code
 * cppu.cpp  cppu.h  example.cpp
 * \endcode
 *
 * <code><b>$ g++ -o test cppu.cpp example.cpp</b></code>
 * 
 * <code><b>$ ls</b></code>
 * \code
 * cppu.cpp  cppu.h  example.cpp  test
 * \endcode
 *
 * <code><b>$ ./test</b></code>
 * \code
=> started test case TC1
example.cpp:15: TC1::testMethod :: 2 not equals 1
example.cpp:21: TC1::testMethod2 :: 2 not equals 1
<= ended test case TC1

==================================================
|               |  failed  |  succeed  |  total  |
|------------------------------------------------|
| assertations: |  2       |  3        |  5      |
| tests:        |  2       |  1        |  3      |
| test cases:   |  0       |  0        |  0      |
| test suites:  |  0       |  0        |  0      |
==================================================

++++++++++++++++++++
=> started test case TC2
example.cpp:42: TC2::testMethod4 :: 2 not equals 1, dude
<= ended test case TC2

==================================================
|               |  failed  |  succeed  |  total  |
|------------------------------------------------|
| assertations: |  1       |  4        |  5      |
| tests:        |  1       |  2        |  3      |
| test cases:   |  0       |  0        |  0      |
| test suites:  |  0       |  0        |  0      |
==================================================

++++++++++++++++++++
=> started test case TC3
<= ended test case TC3

==================================================
|               |  failed  |  succeed  |  total  |
|------------------------------------------------|
| assertations: |  0       |  2        |  2      |
| tests:        |  0       |  2        |  2      |
| test cases:   |  0       |  0        |  0      |
| test suites:  |  0       |  0        |  0      |
==================================================

++++++++++++++++++++
=> started test suite TS1
  => started test case TC1
example.cpp:15: TC1::testMethod :: 2 not equals 1
example.cpp:21: TC1::testMethod2 :: 2 not equals 1
  <= ended test case TC1
  => started test case TC2
example.cpp:42: TC2::testMethod4 :: 2 not equals 1, dude
  <= ended test case TC2
=> ended test suite TS1

==================================================
|               |  failed  |  succeed  |  total  |
|------------------------------------------------|
| assertations: |  3       |  7        |  10     |
| tests:        |  3       |  3        |  6      |
| test cases:   |  2       |  0        |  2      |
| test suites:  |  0       |  0        |  0      |
==================================================

++++++++++++++++++++
=> started test suite TS2
  => started test case TC3
  <= ended test case TC3
=> ended test suite TS2

==================================================
|               |  failed  |  succeed  |  total  |
|------------------------------------------------|
| assertations: |  0       |  2        |  2      |
| tests:        |  0       |  2        |  2      |
| test cases:   |  0       |  1        |  1      |
| test suites:  |  0       |  0        |  0      |
==================================================

++++++++++++++++++++
=> started test suite TS3
  => started test suite TS1
    => started test case TC1
example.cpp:15: TC1::testMethod :: 2 not equals 1
example.cpp:21: TC1::testMethod2 :: 2 not equals 1
    <= ended test case TC1
    => started test case TC2
example.cpp:42: TC2::testMethod4 :: 2 not equals 1, dude
    <= ended test case TC2
  => ended test suite TS1
  => started test case TC2
example.cpp:42: TC2::testMethod4 :: 2 not equals 1, dude
  <= ended test case TC2
=> ended test suite TS3

==================================================
|               |  failed  |  succeed  |  total  |
|------------------------------------------------|
| assertations: |  4       |  11       |  15     |
| tests:        |  4       |  5        |  9      |
| test cases:   |  3       |  0        |  3      |
| test suites:  |  1       |  0        |  1      |
==================================================

++++++++++++++++++++
 * \endcode
 *
 * <code><b>$ cppu_aggregate test</b></code>
 * \code
=> started test case TC1
example.cpp:15: TC1::testMethod :: 2 not equals 1
example.cpp:21: TC1::testMethod2 :: 2 not equals 1
<= ended test case TC1


++++++++++++++++++++
=> started test case TC2
example.cpp:42: TC2::testMethod4 :: 2 not equals 1, dude
<= ended test case TC2


++++++++++++++++++++
=> started test case TC3
<= ended test case TC3


++++++++++++++++++++
=> started test suite TS1
  => started test case TC1
example.cpp:15: TC1::testMethod :: 2 not equals 1
example.cpp:21: TC1::testMethod2 :: 2 not equals 1
  <= ended test case TC1
  => started test case TC2
example.cpp:42: TC2::testMethod4 :: 2 not equals 1, dude
  <= ended test case TC2
=> ended test suite TS1


++++++++++++++++++++
=> started test suite TS2
  => started test case TC3
  <= ended test case TC3
=> ended test suite TS2


++++++++++++++++++++
=> started test suite TS3
  => started test suite TS1
    => started test case TC1
example.cpp:15: TC1::testMethod :: 2 not equals 1
example.cpp:21: TC1::testMethod2 :: 2 not equals 1
    <= ended test case TC1
    => started test case TC2
example.cpp:42: TC2::testMethod4 :: 2 not equals 1, dude
    <= ended test case TC2
  => ended test suite TS1
  => started test case TC2
example.cpp:42: TC2::testMethod4 :: 2 not equals 1, dude
  <= ended test case TC2
=> ended test suite TS3


++++++++++++++++++++

==================================================
|               |  failed  |  succeed  |  total  |
|------------------------------------------------|
| assertations: |  10      |  29       |  39     |
| tests:        |  10      |  15       |  25     |
| test cases:   |  5       |  1        |  6      |
| test suites:  |  1       |  0        |  1      |
==================================================
 * \endcode
 * \endsection
 *
 *
 */

