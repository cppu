GCC = g++
CXXFLAGS = -pedantic -Wall
DEBUGFLAGS = -g

INSTALL = install
RM = rm
DESTDIR = /usr/local

VERSION = 0.3

all: libcppu.a

libcppu.a: cppu.o
	ar cr $@ $^
	ranlib $@

cppu.o: cppu.cpp cppu.h
	$(GCC) $(CXXFLAGS) $(DEBUGFLAGS) -c -o $@ $<

archive: libcppu.a cppu.h
	git archive --prefix=cppu-$(VERSION)/ --format=tar v$(VERSION) | gzip > \
		cppu-$(VERSION).tar.gz

install: libcppu.a cppu.h
	$(INSTALL) -d -m755 $(DESTDIR)/lib
	$(INSTALL) -d -m755 $(DESTDIR)/include
	$(INSTALL) -d -m755 $(DESTDIR)/bin
	$(INSTALL) -m644 libcppu.a $(DESTDIR)/lib/libcppu.a
	$(INSTALL) -m644 cppu.h $(DESTDIR)/include/cppu.h
	$(INSTALL) -m755 cppu-aggregate $(DESTDIR)/bin/cppu-aggregate

uninstall:
	$(RM) $(DESTDIR)/lib/libcppu.a
	$(RM) $(DESTDIR)/include/cppu.h
	$(RM) $(DESTDIR)/bin/cppu-aggregate

doc:
	doxygen doc/doxygen.conf

todo:
	todo cppu.h cppu.cpp

clean:
	rm -f *.o
	rm -f test
	rm -f *.a
	rm -rf doc/html
	rm -f doc/test
	rm -f *.tar.gz

test: doc/test
doc/test: cppu.o doc/example.cpp
	$(GCC) $(CXXFLAGS) $(DEBUGFLAGS) -I./ -o doc/test $^
run_test:
	cd doc && ./test

.PHONY: clean doc install uninstall archive
