/***
 * CppU - C++ unit testing framework
 * ---------------------------------
 * Copyright (c)2007 Daniel Fiser <danfis@danfis.cz>
 *
 *
 *  This file is part of CppU.
 *
 *  CppU is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  CppU is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file cppu.h
 * Header file of CppU.
 */


#ifndef _CPPU_H_
#define _CPPU_H_

#include <string>
#include <vector>
#include <iostream>
/*
 * TODO: Dopsat vsechny aserce (zachytavani vyjimek -
 *       assertException(vyraz, ExceptionType);
 */


/**
 * Struct describing state after ran test.
 */
struct test_state_t{
    std::string test_name; //!< name of test
    int fails; //!< num of failed assertations
    int successes; //!< num of succeed assertations

    inline void clean(void){fails=0;successes=0;}
};

/**
 * Abstract base class.
 *
 * This defines the base methods which must have all classes which handle
 * any tests, test cases and so on.\n
 * Because of this class it's possible to register test suite into other
 * test suite.
 */
class TestBase{
  protected:
    const std::string _name;

    TestBase(const std::string n) : _name(n){}
  public:
    virtual ~TestBase(){}

    /**
     * This runs all tests (and subtests) and print output to out.
     * Parameter level specifies on which level is this test running.
     */
    virtual void run(std::ostream &out, const int level = 0) = 0;

    /*@{*/
    /**
     * \name 
     * This two functions determines whether class is TestCase or
     * TestSuite.
     */
    virtual bool isTestCase() const = 0;
    virtual bool isTestSuite() const = 0;
    /*@}*/


    /*@{*/
    /**
     * \name
     * Methods which returns number of assertations, tests etc.\ 
     * Inheriting class must redefine only methods which make sense
     * for them.
     */
    virtual int assertations() const { return 0;}
    virtual int assertationsFailed() const { return 0;}
    virtual int assertationsSucceed() const { return 0;}
    virtual int testSuites() const { return 0;}
    virtual int testSuitesFailed() const { return 0;}
    virtual int testSuitesSucceed() const { return 0;}
    virtual int testCases() const { return 0;}
    virtual int testCasesFailed() const { return 0;}
    virtual int testCasesSucceed() const { return 0;}
    virtual int tests() const { return 0;}
    virtual int testsFailed() const { return 0;}
    virtual int testsSucceed() const { return 0;}
    /*@}*/

    /**
     * This method prints summary independently on type of class (TestCase
     * or TestSuite).
     * Class which inherits from TestBase must redefine only methods which
     * count assertations, tests etc...
     */
    virtual void printSummary(std::ostream &out = std::cout) const;
};



/**
 * Main test case class.
 * This class collects all individual tests in one test case.
 */
class TestCase : public TestBase{
  protected:
    int _tests_failed; //!< number of failed tests
    int _tests_succeed; //!< number of succeed tests
    int _assertations_failed; //!< number of failed assertations
    int _assertations_succeed; //!< number of succeed assertations

    /**
     * State of currently running test.
     * This struct must be empty before running next test method.
     */
    test_state_t _current_test;

    /**
     * Which output stream has to be used.
     */
    std::ostream *_out;

    
    /**
     * Returns formated error message
     */
    std::string _getErrorMessage(const std::string file, const int line,
            const std::string test_method,
            const std::string error_message) const;


    /*@{*/
    /**
     * \name
     * Usage:
     * \code
     *   _prepareTest("nameOfTheTestMethod");
     *   testMethod();
     *   _finishTest();
     * \endcode
     */
    void _prepareTest(std::string test_name);
    void _finishTest();
    /*@}*/

    /**
     * Record assertation as succeed
     */
    void _successAssertation();

    /**
     * Record assertations as failed and print error message to stdout.
     */
    void _failAssertation(std::string file, int line,
            std::string error_message);

    /**
     * Constructor is protected because new test case (which inherits from
     * TestCase class) defines its own constructor which only call
     * this one with its name as argument.
     */
    explicit TestCase(const std::string& n) : TestBase(n),
            _tests_failed(0), _tests_succeed(0),
            _assertations_failed(0), _assertations_succeed(0){}
  public:
    virtual ~TestCase(){}

    bool isTestCase() const { return true;}
    bool isTestSuite() const { return false;}

    virtual void setUp(){}
    virtual void tearDown(){}

    /**
     * Method describing tests.\ 
     * This must be redefined in extended class.
     */
    virtual void runTests() = 0;

    /**
     * This run the test case.
     * If verbose is set to true, errors will be printed out during
     * test case running.
     */
    void run(std::ostream &out = std::cout, const int level = 0);


    int assertations() const { return _assertations_failed +
        _assertations_succeed;}
    int assertationsFailed() const { return _assertations_failed;}
    int assertationsSucceed() const { return _assertations_succeed;}
    int tests() const {return _tests_failed + _tests_succeed;}
    int testsFailed() const {return _tests_failed;}
    int testsSucceed() const {return _tests_succeed;}
};



/**
 * Main test suite class.
 * This class collect all test cases and run them together.\n
 * TestSuite can collect and mix test cases and other test suites together,
 * so it's possible to create hirearchy of tests.\n
 * Test cases and test suites are run in order they are registered.
 */
class TestSuite : public TestBase{
  protected:
    /**
     * List of registered tests.
     */
    std::vector<TestBase *> _tests;

    /**
     * Register test cases and test suites for later run.
     */
    void _reg(TestBase *tc){ _tests.push_back(tc); }

    /**
     * Erase all tests from vector.
     */
    void _freeTests();

    /**
     * Inheriting class must redefine this method.\ 
     * In this method should be only list of _reg() methods calls.\ 
     * This method is ran in run() method as first.
     */
    virtual void _registerAll() = 0;
  public:
    TestSuite(const std::string& n) : TestBase(n){}
    virtual ~TestSuite();

    bool isTestCase() const { return false;}
    bool isTestSuite() const { return true;}

    /**
     * This run the tests.
     * If verbose is set to true, errors will be printed out during
     * test case running.
     */
    void run(std::ostream &out = std::cout, const int level = 0);


    int assertations() const;
    int assertationsFailed() const;
    int assertationsSucceed() const;
    int testSuites() const;
    int testSuitesFailed() const;
    int testSuitesSucceed() const;
    int testCases() const;
    int testCasesFailed() const;
    int testCasesSucceed() const;
    int tests() const;
    int testsFailed() const;
    int testsSucceed() const;
};


/**
 * Function which runs test (given in argument) and all
 * outputs go into out.
 */
void runTest(TestBase * test, std::ostream &out = std::cout);

/**************  MACROS  ****************/
/**********  TestCase **********/
/**
 * Create test case.\ This macro must be enclosed by TEST_CASE_END macro.
 */
#define TEST_CASE(name) \
    class name : public TestCase{ \
      public: \
        name() : TestCase(#name){}
/**
 * Close definition of test case.
 */
#define TEST_CASE_END }

/**
 * Introduce block where has to be all test methods registered.
 */
#define TESTS void runTests()

/**
 * Register method in TESTS block.
 */
#define REG_TEST(test_name) \
    _prepareTest(#test_name); \
    this->test_name(); \
    _finishTest();


/**********  TestSuite **********/
/**
 * Create test suite.\ This macro has to be enclosed by TEST_SUITE_END
 * macro.
 */
#define TEST_SUITE(name) \
    class name : public TestSuite{ \
      public: \
        name() : TestSuite(#name){} \
        void _registerAll() \
        {
/**
 * Close definition of test suite.
 */
#define TEST_SUITE_END \
        } \
    }
/**
 * Register test case or test suite into test suite.\ This macro has to be
 * between TEST_SUITE and TEST_SUITE_END macro.
 */
#define REG(name) \
            _reg(new name());

    

/**
 * Runs test suite or test case.\ Output goes to stdout.
 */
#define RUN(name) \
    runTest(new name());
/**
 * Runs test suite or test case.\ Output goes to specified out.
 */
#define RUN_OUT(name, out) \
    runTest(new name(), out);
    
/**********  assertations **********/
/*@{*/
/**
 * \name Assertations
 * Assertations with suffix 'M' (e.g.\ assertTrueM) is variation of macro
 * where is possible to specify error message.
 */
#define assertTrueM(a, message) \
    if (a){ \
        _successAssertation(); \
    }else{ \
        _failAssertation(__FILE__, __LINE__, message); \
    }
#define assertTrue(a) \
    assertTrueM((a), #a " is not true")

#define assertFalseM(a, message) \
    assertTrueM(!(a), message)
#define assertFalse(a) \
    assertFalseM((a), #a " is not false")

#define assertEqualsM(a,b,message) \
    assertTrueM((a) == (b), message)
#define assertEquals(a,b) \
    assertEqualsM((a), (b), #a " not equals " #b)

#define assertNotEqualsM(a,b,message) \
    assertTrueM((a) != (b), message)
#define assertNotEquals(a,b) \
    assertNotEqualsM((a), (b), #a " equals " #b)

#define assertThrow(expr, except) \
    try { \
        (expr); \
        assertTrueM(false, #except " is not throwed - nothing throwed"); \
    }catch(except e){ \
    }catch(std::exception &e){ \
        assertTrueM(false, #except " is not throwed, but std::exception throwed"); \
    }catch(std::string &e){ \
        assertTrueM(false, #except " is not throwed, but std::string throwed"); \
    }
#endif
/*@}*/
/* vim: set sw=4 ts=4 et ft=cpp tw=75 cindent: */
